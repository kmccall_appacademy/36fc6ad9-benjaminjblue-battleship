class Board
  attr_reader :grid, :ship_count

  def self.default_grid
    (0...10).map{|_| (0...10).map{|_| nil }}
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
    @ship_count = 0
  end

  def [](pos)
    @grid[pos[0]][pos[1]]
  end

  def display
    board = @grid.reduce("") do |total, row|
      total + row.reduce("") do |row_total, cell|
        tile = ((cell == nil || cell == :s) ? "~" : cell.to_s) + " "
        row_total + tile
      end + "\n"
    end
    puts board
  end

  def show_ships
    board = @grid.reduce("") do |total, row|
      total + row.reduce("") do |row_total, cell|
        tile = ((cell == nil) ? "~" : cell.to_s) + " "
        row_total + tile
      end + "\n"
    end
    puts board
  end

  def count
    total = @grid.reduce(0) do |total, row|
      total + row.reduce(0) do |row_total, cell|
        (cell == :s ? 1 : 0) + row_total
      end
    end
  end

  def or?(comparee, *args)
    args.any? {|arg| arg == comparee}
  end

  def empty?(pos = nil)
    if pos
      or?(@grid[pos[0]][pos[1]], nil, :-)
    else
      !@grid.any?{|row| row.any?{|cell| cell != nil}}
    end
  end

  def full?
    @grid.all?{|row| row.all?{|cell| cell != nil}}
  end

  def place_random_ship
    empty_pos = []
    @grid.each_with_index do |row, i|
      row.each_with_index do |cell, j|
        empty_pos.push([i,j]) if !cell
      end
    end
    if empty_pos.length > 0
      rand_pos = empty_pos[rand(empty_pos.length)]
      @grid[rand_pos[0]][rand_pos[1]] = :s
    else
      raise "can't place boat on full board"
    end
  end

  def place_ship(pos, length, orientation)
    if valid_placement?(pos, length, orientation)
      (0...length).each do |i|
        x, y = (orientation == "h" ? i : 0), (orientation == "v" ? i : 0)
        @grid[pos[0] + y][pos[1] + x] = :s
      end
    else
      false
    end
  end

  def valid_placement?(pos, length, orientation)
    if possible_fit?(pos, length, orientation)
      are_spaces_empty = (0...length).all? do |i|
        x, y = (orientation == "h" ? i : 0), (orientation == "v" ? i : 0)
        empty?([pos[0] + y, pos[1] + x])
      end
    else
      false
    end
  end

  def possible_fit?(pos, length, orientation)
    width = @grid[0].length
    height = @grid.length
    x_extension = (orientation == "h" ? length : 0)
    y_extension = (orientation == "v" ? length : 0)
    pos[0] + y_extension <= height && pos[1] + x_extension <= width
  end

  def won?
    @grid.all?{|row| row.all?{|cell| cell != :s}}
  end

  def populate_grid
    ship_lengths = [5, 4, 3, 3, 2]
    ship_lengths.each do |length|
      is_valid_placement = false
      while !is_valid_placement
        orientation = ["h", "v"][rand(2)]
        pos = [rand(@grid.length), rand(@grid[0].length)]
        is_valid_placement = self.place_ship(pos, length, orientation)
      end
    end
  end

  def manually_populate_grid(player)
    ship_lengths = [5, 4, 3, 3, 2]
    ship_lengths.each do |length|
      is_valid_placement = false

      while !is_valid_placement
        self.show_ships
        puts "ship length: #{length}"
        orientation = player.get_orientation
        pos = player.get_play(self)
        is_valid_placement = self.place_ship(pos, length, orientation)
        puts "invalid placement" if !is_valid_placement
      end
    end
  end
end

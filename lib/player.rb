class HumanPlayer
  attr_reader :atk_counter

  def initialize
    @atk_counter = 0
  end

  def get_play(board)

    grid = board.grid
    puts "enter a coordinate: 'x, y' (range of 0-#{grid.length - 1}, 0-#{grid[0].length - 1})"
    move = gets.chomp

    can_split = move.include?(", ")

    if can_split

      pos = move.split(", ").map(&:to_i)

      if pos.all?{|coord| coord.is_a?(Integer)}

        if pos[0] < grid.length && pos[1] < grid[0].length

          pos

        else

          puts "out of range"
          self.get_play(board)

        end

      else

        puts "invalid format, enter 'x, y'"
        self.get_play(board)

      end

    else

      puts "invalid format"
      self.get_play(board)

    end
  end

  def get_orientation
    puts "Would you like to place this boat horizontal (h) or vertical (v)?"
    orientation = gets.chomp
    if orientation == "h"
      return "h"
    elsif orientation == "v"
      return "v"
    else
      get_orientation
    end
  end

  def atk_count_increment
    @atk_counter += 1
  end

  def atk_count
    @atk_counter
  end

end

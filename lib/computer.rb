class ComputerPlayer
  attr_reader :attacks, :atk_count
  def initialize
    @attacks = []
    @atk_counter = 0
  end

  def get_play(board)
    grid = board.grid
    all_moves = (0...grid.length).map{|row| (0...grid[0].length).map{|col| [row, col]}}.flatten(1)
    possible_moves = all_moves.select{|pos| !@attacks.include?(pos)}
    move = possible_moves[rand(possible_moves.length)]
    @attacks.push(move)
    move
  end

  def atk_count_increment
    @atk_counter += 1
  end

  def atk_count
    @atk_counter
  end
end

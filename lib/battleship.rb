require "./board"
require "./player"
require "./computer"

class BattleshipGame
  attr_reader :board1, :player1, :board2, :player2, :current_player, :current_board

  def initialize(player1, board1, player2, board2)
    @player1 = player1
    @board1 = board1
    @player2 = player2
    @board2 = board2
    @current_player = @player1
    @current_board = @board2
  end

  def play
    ships_left = true
    set_up(@board1, @player1)
    set_up(@board2, @player2)
    puts "Game on!"
    while ships_left
      self.display_status
      @current_board.display
      self.play_turn
      ships_left = !game_over?
      switch_players
    end

    switch_players
    puts (@current_player.is_a?(HumanPlayer) ? "You won!" : "You lost!")
    self.display_status
    @current_board.display
  end

  def switch_players
    @current_player = (@current_player == @player1 ? @player2 : @player1)
    @current_board = (@current_board == @board1 ? @board2 : @board1)
  end

  def attack(pos)
    @current_board.grid[pos[0]][pos[1]] = (@current_board.empty?(pos) ? :x : :o)
  end

  def count
    @current_board.count
  end

  def game_over?
    @current_board.won?
  end

  def play_turn
    move = @current_player.get_play(@current_board)
    self.attack(move)
    @current_player.atk_count_increment
  end

  def display_status
    puts "attacks launched: #{@current_player.atk_count}"
    puts "ships left: #{self.count}"
    puts "computer's board" if @current_player.is_a?(ComputerPlayer)
  end

  def set_up(board, player)
    if player.is_a?(HumanPlayer)
      manual_set_up(board, player)
    else
      board.populate_grid
    end
  end

  def manual_set_up(board, player)
    puts "Would you like to set up the ships yourself?"
    if gets.chomp.downcase[0...3] == "yes"
      board.manually_populate_grid(player)
    else
      board.populate_grid
    end
  end

end

player1 = HumanPlayer.new
board1 = Board.new
player2 = ComputerPlayer.new
board2 = Board.new
game = BattleshipGame.new(player1, board1, player2, board2)
game.play
